#![feature(libc)]
extern crate libc;
extern crate win32_types;

use libc::c_int;
use win32_types::{HMODULE, LPCWSTR, LPCSTR, DWORD, BOOL, LARGE_INTEGER, ULONGLONG};

// TODO should it not be c convention?
pub type FARPROC = unsafe extern "stdcall" fn () -> c_int;

#[link(name = "kernel32")]
extern "stdcall" {
    pub fn QueryPerformanceFrequency(lpFrequency: *mut LARGE_INTEGER) -> BOOL;

    pub fn QueryPerformanceCounter(lpPerformanceCount: *mut LARGE_INTEGER) -> BOOL;

    pub fn GetTickCount64() -> ULONGLONG;

    pub fn GetModuleHandleW(lpModuleName: LPCWSTR) -> HMODULE;

    pub fn GetLastError() -> DWORD;

    pub fn LoadLibraryW(lpFileName: LPCWSTR) -> HMODULE;

    pub fn FreeLibrary(hModule: HMODULE) -> BOOL;

    pub fn GetProcAddress(hModule: HMODULE, lpProcName: LPCSTR) -> FARPROC;
}
